<?php 
include_once '../model/bean/Pessoa.php';
include_once '../model/bean/Usuario.php';
include_once 'AbstractDao.php';

class FormularioDao extends AbstractDao{ 
    
    
    public function inserirPessoa(Pessoa $pessoa){
             $sql = parent::getConexao()->action()->prepare("INSERT INTO curso_pessoa (nome,email) VALUES(:nome,:email)");
             $sql->bindParam(':nome', $pessoa->getNome());
             $sql->bindParam(':email', $pessoa->getEmail());
             $sql->execute();
        
    }
    
    
    public function inserirUsuario(Usuario $usuario, Pessoa $pessoa){
        
             $idPessoa = self::selecionarPorNomeEmail($pessoa);    
             $sql = parent::getConexao()->action()->prepare("INSERT INTO curso_usuario (senha,login,idPessoa) VALUES(:senha,:login,:idPessoa)");
             $sql->bindParam(':login',$usuario->getLogin());
             $sql->bindParam(':senha',$usuario->getSenha());
             $sql->bindParam(':idPessoa', $idPessoa['id']);
             $sql->execute();
             
    }
    
    public function cadastrar(Usuario $usuario,Pessoa $pessoa){
        try{
            self::inserirPessoa($pessoa);
            self::inserirUsuario($usuario, $pessoa);
            return true;
        }
        catch (PDOStatement $e){
            return false;
        }
        
    }
    
    public function selecionarPorNomeEmail(Pessoa $pessoa){
        $sql = parent::getConexao()->action()->query("select id from curso_pessoa WHERE nome = '".$pessoa->getNome()."' and email =  '".$pessoa->getEmail()."'");
        $pessoaId =  $sql->fetch(PDO::FETCH_ASSOC);
        return  $pessoaId;
    }
}




?>